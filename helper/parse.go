package helper

import (
	"fmt"
	"regexp"
)

// ParseEmail 解析邮件地址，提取用户名、二级域名、一级域名
func ParseEmail(emailAddr string) (username, domain, tld string, err error) {
	// 定义用于匹配邮箱地址的正则表达式
	emailRegex := regexp.MustCompile(EmailAddrReg)
	match := emailRegex.FindStringSubmatch(emailAddr)
	if len(match) == 0 {
		err = fmt.Errorf("无效的邮件地址")
		return
	}
	fmt.Println(match)
	username = match[1]
	domain = match[2]
	tld = match[3]
	return
}
