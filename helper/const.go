package helper

import "time"

const (
	// VideoBucketName ImageBucketName MinioRegion minio 相关
	VideoBucketName     = "alarm.video"
	ImageBucketName     = "alarm.image"
	MinioRegion         = "cn-north-1"
	GlobalBaseConfigKey = "global_base_config"

	// Default RPCCodeSuc 状态码
	RPCCodeSuc    = 10200
	RPCCodeFailed = 10201

	// Default RPCInfo rpc信息
	RPCMsgSuc               = "消息接收成功"
	RPCMsgFailed            = "告警消息内容不全"
	RPCMsgPredictInfoFailed = "推理机信息收集不全"
	RPCMsgTaskCloseFailed   = "任务关闭失败"
	RPCMsgTaskCloseOpen     = "任务开启失败"
	RpcServicePredict       = "predict"
	RpcServiceSchedule      = "schedule"
	RpcServiceGateway       = "gateway"
	RpcEtcdPredictErr       = "发现etcd的predict服务失败"
	RpcPredictNewClientErr  = "创建推理机RPC客户端失败"
	RpcPredictTaskOpenErr   = "分配任务给推理机失败"
	RpcRegisterErr          = "注册服务到etcd失败"
	RpcConnEtcdErr          = "rpc kitex 连接etcd失败"
	RpcGateNewClientErr     = "创建gateRPC客户端失败"

	// for gateway and video
	VideoPreviewUrlSplitStr = "ISSTECHVIDEOSPLIT"

	// AlarmInfoErr 告警消息处理
	AlarmInfoErr = "推理机发送的告警信息不全"
	AlarmSubject = "alarm"
	AlarmQueue   = "queue01"

	HttpClientTimeOut   = 5
	NoticeStatusDefault = 0
	NoticeStatusSuc     = 1
	NoticeStatusFailed  = 2

	MinioContentImg   = "image/jpeg"
	MinioContentVideo = "video/mp4"
	MinioDefaultDay   = time.Hour * 24 * 7 // 七天

	TaskStatusClose = 0
	TaskStatusOpen  = 1

	//推理机状态
	PredictStatusOpen  = 1001
	PredictStatusClose = 1002

	//----------------------------正则验证相关--------------------------
	// 常用国际电话号码区号
	ChinaMainlandPhNumberRegion = "+86"  // 中国大陆电话区号
	ChinaMacauPhNumberRegion    = "+853" // 中国澳门电话区号
	ChinaHongKongPhNumberRegion = "+852" // 中国香港电话区号
	ChinaTaiwanPhNumberRegion   = "+886" // 中国台湾电话区号
	MalaysiaPhNumberRegion      = "+60"  // 马来西亚电话区号
	DubaiPhNumberRegion         = "+971" // 迪拜，阿联酋 电话区号
	PhilippinesPhNumberRegion   = "+63"  // 菲律宾电话区号

	// 车牌号类型定义 包含 油车与新能源（混动+纯电）
	CarPlateICEType = "1" // 油车
	CarPlateENVType = "2" // 新能源

	// 常用正则表达式
	IPOrDomainReg            = `(([a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}|((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))`                                                                                                                                                                                                                // ip 或 域名的检测
	UsernameReg              = `^[a-zA-Z0-9_]{4,}$`                                                                                                                                                                                                                                                                                                                                                 // 登录账号 由字母、数字、下划线组成的字符串
	NicknameReg              = "^[\u4e00-\u9fa5a-zA-Z0-9_]{1,}$"                                                                                                                                                                                                                                                                                                                                    // 昵称 由汉字、字母、数字组成
	PasswordReg              = `^[a-zA-Z0-9!~@#$%^&*)(_+.-={}|]{2,}$`                                                                                                                                                                                                                                                                                                                               // 登录密码 由字母、数字、特殊字符等组成
	EmailAddrReg             = `^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$`                                                                                                                                                                                                              // 邮件地址
	ChinaMainlandIDReg       = `^\d{6}((((((19|20)\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(((19|20)\d{2})(0[13578]|1[02])31)|((19|20)\d{2})02(0[1-9]|1\d|2[0-8])|((((19|20)([13579][26]|[2468][048]|0[48]))|(2000))0229))\d{3})|((((\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|((\d{2})(0[13578]|1[02])31)|((\d{2})02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[048])0229))\d{2}))(\d|X|x)$` // 大陆身份证 支持15和18位
	ChinesHkMacaoPspReg      = `^[EeKkGgDdSsPpHh]\d{8}$)|(^(([Ee][a-fA-F])|([DdSsPp][Ee])|([Kk][Jj])|([Mm][Aa])|(1[45]))\d{7}$`                                                                                                                                                                                                                                                                     // 香港 澳门的护照
	ChinaMainlandPhNumberReg = `^[1][3-9][0-9]{9}$`                                                                                                                                                                                                                                                                                                                                                 // 大陆电话号码
	ChinaMacauPhNumberReg    = `^\d{8}$`                                                                                                                                                                                                                                                                                                                                                            // 中国澳门电话格式
	ChinaHongKongPhNumberReg = `^[56789]\d{7}$`                                                                                                                                                                                                                                                                                                                                                     // 中国香港电话格式
	ChinaTaiwanPhNumberReg   = `^09\d{8}$`                                                                                                                                                                                                                                                                                                                                                          // 中国台湾电话格式
	MalaysiaPhNumberReg      = `^(?:(?:0[1-9]\d{2})\)?[ -]?\d{7,8})$`                                                                                                                                                                                                                                                                                                                               // 马来西亚电话格式
	DubaiPhNumberReg         = `^971[ -]?(?:\d{1,3})[ -]?\d{7,8}$`                                                                                                                                                                                                                                                                                                                                  // 迪拜 阿联酋电话格式
	PhilippinesPhNumberReg   = `^63[ -]?(?:\d{2,3})[ -]?\d{7,8}$`                                                                                                                                                                                                                                                                                                                                   // 菲律宾电话格式
	CarPlateNumberICEReg     = `^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-HJ-NP-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]$`                                                                                                                                                                                                                                                                               // Car Plate Number 车牌号（油车）
	CarPlateNumberNEVReg     = `^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-HJ-NP-Z](?:((\d{5}[A-HJK])|([A-HJK][A-HJ-NP-Z0-9][0-9]{4}))|[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳])$`                                                                                                                                                                                                                          // 车牌号，新能源 混动+纯电
	GUIDOrUUIDReg            = `^[a-f\d]{4}(?:[a-f\d]{4}-){4}[a-f\d]{12}$`                                                                                                                                                                                                                                                                                                                          // UUID 或 GUID
)