package helper

// 全局基础配置-放在etcd用于后台配置动态更新
type GlobalBaseConfig struct {
	/* 这里是详情描述
		for video rtsp和rtmp 每个视频服务都会做视频转码，转码后的地址供推理端使用，多个视频服务时需要配置多个，
		video服务在本地配置文件中有video_id的编号，从0开始，按此id获取数组中的暴漏地址
		 例如：["1.1.1.1:1935","1.1.1.1:1935","1.1.1.2:1935"] 配置文件中video_id=0将匹配到1.1.1.1:1935

		H5LiveAddress: 给h5页面访问的视频地址，逻辑同上，此地址是客户端所在机器能访问到的，如果是代理后
		的ip:port 则注意代理配置内容关系是 video真实ip:port代理到nginx外网ip ，我们配置的是nginx外网ip:port

		MinioPreviewAddress: 默认写的是minio服务所在ip:port 如果是代理后的ip:port 则注意代理配置内容关系是
		minio真实ip:port代理到nginx外网ip,我们配置的是nginx外网ip:port

		RecordVideoDuration: 录制的视频时长，单位秒，默认10s，取值范围10-60s
	    FfmpegBinPath: 引用的ffmpeg命令路径 "ffmpeg_bin_path": "ffmpeg"
		LogFileKeepDays: 服务产生的日志文件保留天数-不包含审计日志，审计日志已入库不受影响
	*/
	RtspExportAddress         []string `json:"rtsp_export_address" yaml:"rtsp_export_address"`                     // 给推理端内部的rtsp地址
	RtmpExportAddress         []string `json:"rtmp_export_address" yaml:"rtmp_export_address"`                     // 给推理端内部的rtmp地址
	H5LiveAddress             []string `json:"h5_live_address" yaml:"h5_live_address"`                             // 前端页面播放直播流的地址
	MinioPreviewAddress       string   `json:"minio_preview_address" yaml:"minio_preview_address"`                 // 告警图片和视频浏览地址
	FfmpegBinPath             string   `json:"ffmpeg_bin_path" yaml:"ffmpeg_bin_path"`                             // 引用的ffmpeg命令路径
	RecordPath                string   `json:"record_path" yaml:"record_path"`                                     // 设置录制视频的路径：record/hls
	RecordVideoDuration       uint8    `json:"record_video_duration" yaml:"record_video_duration"`                 // 录制的视频时长，单位秒，默认10s
	LogFileExpirationDays     uint8    `json:"log_file_expiration_days" yaml:"log_file_expiration_days"`           // 日志文件过期时间
	MinioDataExpirationDays   uint16   `json:"minio_data_expiration_days" yaml:"minio_data_expiration_days"`       // minio中数据过期时间
	TaskAlgCountLimit         uint8    `json:"task_alg_count_limit" yaml:"task_alg_count_limit"`                   // 每个任务允许的算法个数，取值范围1-10，默认3
	ThirdPartyVideoSourceType uint8    `json:"third_party_video_source_type" yaml:"third_party_video_source_type"` // 第三方视频源同步类型，0不开启，1华为，2海康
	ThirdPartyAddress         []string `json:"third_party_address" yaml:"third_party_address"`                     // 格式："ip:port,ip:port"支持视频集群地址
	ThirdPartyUserName        string   `json:"third_party_user_name" yaml:"third_party_user_name"`                 // 用户名，用于获取第三方验证token或session的
	ThirdPartyUserPwd         string   `json:"third_party_user_pwd" yaml:"third_party_user_pwd"`                   // 用户密码，用于获取第三方验证token或session的
}

/*
获取延迟关闭任务时间
延迟关闭任务的目的：避免关闭视频流时发生告警引起视频无法录制到事件的后N秒
*/
func GetDelayCloseTask(recordVideoDuration uint8) int {
	// /举例：录制10s的视频 当前时间10:01:05 那么视频应该是10:01:00-10:01:10则最少等待5s中才能录制到10：01：10的文件
	//  一般我们设置N+2（2是冗余时长）// 如果录制30s，则是30/2+2=17秒
	return int(recordVideoDuration)/2 + 2
}

/*
获取关闭任务后间隔开启任务的时间
开启任务时要检查上次关闭视频流的时间，目的是：避免因为延迟关闭而导致刚开启的任务视频流被关闭
例如：视频A的任务1 在10:01:00关闭了，但实际它将在N(5)秒后关闭视频流，如果马上开启任务，
与上次关闭视频流时间小于10:01:00+5 那在05秒的时候就会关闭05秒之前开启的任务，导致任务失败
*/
func GetTaskIntervalTime(recordVideoDuration uint8) int {
	return GetDelayCloseTask(recordVideoDuration)
}

// 获取合并视频的延迟时间
func GetMergeVideoTimeWait(recordVideoDuration uint8) int {
	return GetDelayCloseTask(recordVideoDuration)
}

// 获取删除历史录制视频文件的时间
func GetDeleteOldRecord(recordVideoDuration uint8) int {
	return GetDelayCloseTask(recordVideoDuration)
}
