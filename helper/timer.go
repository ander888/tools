package helper

import (
	"strconv"
	"time"
)

// GetCurTime 获取当前时间毫秒级和13位的时间戳,UTC ,数组截取时为了避免尾数四舍五入的情况
func GetCurTime13bit() (now time.Time, ts int64) {
	ts, _ = strconv.ParseInt(strconv.FormatInt(time.Now().UnixNano(),
		10)[:13], 10, 64)
	now = time.Unix(0, ts*1000000).UTC()
	return
}

// 获取当前时间和对应的10位int64类型的时间戳
func GetCurTime10bit() (now time.Time, ts int64) {
	ts, _ = strconv.ParseInt(strconv.FormatInt(time.Now().UnixNano(),
		10)[:13], 10, 64)
	now = time.Unix(0, ts*1000000).UTC()
	return
}

// 将时间转为13位的字符串类型，多用于日志或数据入库
func TimeToTimeStampStr13bit(t time.Time) string {
	return strconv.FormatInt(t.UTC().UnixNano(),
		10)[:13]
}

// 将传入的时间转为13位的int64类型，多用于时间比较
func TimeToTimeStampInt6413bit(t time.Time) int64 {
	ts, _ := strconv.ParseInt(strconv.FormatInt(t.UTC().UnixNano(),
		10)[:13], 10, 64)
	return ts
}

// 将传入的时间转为10位的int类型，多用于时间比较
func TimeToTimeStampInt10bit(t time.Time) int32 {
	ts, _ := strconv.Atoi(strconv.FormatInt(t.UTC().UnixNano(),
		10)[:10])
	return int32(ts)
}

// 将时间戳转为时间类型
func TimeStampToTime(ts int64) time.Time {
	return time.Unix(0, ts*1000000).UTC()
}

// 获取字符串类型的当前时间
func GetCurTimeStr() (nowStr string) {
	ts, _ := strconv.ParseInt(strconv.FormatInt(time.Now().UnixNano(),
		10)[:13], 10, 64)
	nowStr = time.Unix(0, ts*1000000).UTC().String()
	return
}
