package helper

import (
	"fmt"
	"testing"

	"gitee.com/ander888/tools/zlog"
)

func TestReadConf(t *testing.T) {
	//c := GetGatewayConfByYml("")
	_, _, err := InitVideoConn("../conf/video_default.yaml")
	fmt.Printf("%v\n", err)
	// c := GetVideoConfByYml("")
	// fmt.Printf("%-v", c.EtcdConf.Nodes[0])
	//c := GetScheduleConfByYml("")
	// c := GetPredictConfByYml("")
	// var sc VideoConfSt
	// GetYamlConf("../conf/video_default.yaml", &sc)
	// fmt.Printf("config %+v\n", sc.EtcdConf.Nodes)
}

func TestSnowErr(t *testing.T) {
	sn, err := NewIdWorker(3, 1)
	if err != nil {
		println(err.Error())
		return
	}
	for i := 0; i < 10000; i++ {
		println(sn.GetIdStr())
	}

}

func TestCheckHttpUrl(t *testing.T) {
	httpUrl1 := "http://12.31.3.1:65536/aaa"
	zlog.InfoWithStr().Any("匹配结果：", ValHttpUrl(httpUrl1)).Msg(httpUrl1)
}
func TestCheckIPOrDomain(t *testing.T) {
	ipOrD := "1.1.1.256"
	zlog.InfoWithStr().Any("匹配结果：", ValIPOrDomain(ipOrD)).Msg(ipOrD)
}

func TestCheckIPOrDomainWithPort(t *testing.T) {
	ipOrDWithPort := "www.aa11baidu.com:900"
	zlog.InfoWithStr().Any("匹配结果：", ValIPOrDomainWithPort(ipOrDWithPort)).Msg(ipOrDWithPort)
}

func TestValUserName(t *testing.T) {
	un := "1234"
	zlog.InfoWithStr().Any("匹配结果：", ValUserName(un, 10)).Msg(un)
}

func TestValNickName(t *testing.T) {
	nn := "喘气"
	zlog.InfoWithStr().Any("匹配结果：", ValNickname(nn, 10)).Msg(nn)
}

func TestValEmail(t *testing.T) {
	emailAddr := "110@g.ccc"
	zlog.InfoWithStr().Any("匹配结果：", ValEmailAddr(emailAddr)).Msg(emailAddr)
}

func TestValCarPlNm(t *testing.T) {
	carN := "京A88888"
	_, tp := ValCarPlateNumber(carN)
	zlog.Info("车牌类型： " + tp)
}
func TestParseEmailAddr(t *testing.T) {
	emailAddr := "a@gmail.com"
	uName, domain, tls, err := ParseEmail(emailAddr)
	if err != nil {
		zlog.Err(err, EmailAddrReg)
		return
	}
	zlog.InfoWithStr().Any("匹配结果：", uName+"-----"+domain+"-----"+tls).Msg(emailAddr)
}
