package helper

import (
	"os"

	"gitee.com/ander888/tools/model"
	"gitee.com/ander888/tools/zlog"
	"gopkg.in/yaml.v3"
)

type GatewayConfSt struct {
	EtcdConf model.EtcdConf  `json:"etcd_conf" yaml:"etcd_conf"`
	SqlCnf   model.SQLConf   `json:"sql_cnf" yaml:"sql_cnf"`
	MinioCnf model.MinioConf `json:"minio_cnf" yaml:"minio_cnf"`
	LogCnf   model.LogConf   `json:"log_cnf" yaml:"log_cnf"`
}

type VideoConfSt struct {
	EtcdConf model.EtcdConf  `json:"etcd_conf" yaml:"etcd_conf"`
	MinioCnf model.MinioConf `json:"minio_cnf" yaml:"minio_cnf"`
	LogCnf   model.LogConf   `json:"log_cnf" yaml:"log_cnf"`
}

type ScheduleConfSt struct {
	EtcdConf model.EtcdConf  `json:"etcd_conf" yaml:"etcd_conf"`
	MinioCnf model.MinioConf `json:"minio_cnf" yaml:"minio_cnf"`
	LogCnf   model.LogConf   `json:"log_cnf" yaml:"log_cnf"`
}

type PredictConfSt struct {
	EtcdConf model.EtcdConf `json:"etcd_conf" yaml:"etcd_conf"`
	LogCnf   model.LogConf  `json:"log_cnf" yaml:"log_cnf"`
}

// GetGatewayConfByYml 按配置文件获取配置信息
func GetGatewayConfByYml(cnfPath string) (env GatewayConfSt) {
	if len(cnfPath) == 0 {
		cnfPath = "../conf/gateway_default.yaml"
	}

	dataBytes, err := os.ReadFile(cnfPath)
	CheckErrorWithPanic(err, "读取配置文件失败，请检查配置文件：", cnfPath)
	err = yaml.Unmarshal(dataBytes, &env)
	CheckErrorWithPanic(err, "反序列化配置文件失败：", env)
	return env
}

// GetVideoConfByYml 按配置文件获取配置信息
func GetVideoConfByYml(cnfPath string) (env VideoConfSt) {
	if len(cnfPath) == 0 {
		cnfPath = "../conf/video_default.yaml"
	}
	dataBytes, err := os.ReadFile(cnfPath)
	CheckErrorWithPanic(err, "读取配置文件失败，请检查配置文件：", cnfPath)
	err = yaml.Unmarshal(dataBytes, &env)
	CheckErrorWithPanic(err, "反序列化配置文件失败：", env)
	return env
}

// GetScheduleConfByYml 按配置文件获取配置信息
func GetScheduleConfByYml(cnfPath string) (env ScheduleConfSt) {
	if len(cnfPath) == 0 {
		cnfPath = "../conf/schedule_default.yaml"
	}

	dataBytes, err := os.ReadFile(cnfPath)
	CheckErrorWithPanic(err, "读取配置文件失败，请检查配置文件：", cnfPath)
	err = yaml.Unmarshal(dataBytes, &env)
	CheckErrorWithPanic(err, "反序列化配置文件失败：", env)
	return env
}

// GetPredictConfByYml 按配置文件获取配置信息
func GetPredictConfByYml(cnfPath string) (env PredictConfSt) {
	if len(cnfPath) == 0 {
		cnfPath = "../conf/predict_default.yaml"
	}

	dataBytes, err := os.ReadFile(cnfPath)
	CheckErrorWithPanic(err, "读取配置文件失败，请检查配置文件：", cnfPath)
	err = yaml.Unmarshal(dataBytes, &env)
	CheckErrorWithPanic(err, "反序列化配置文件失败：", env)
	return env
}

func GetYamlConf(cnfPath string, configStruct interface{}) {
	// 设置默认配置文件
	if !fileExists(cnfPath) {
		zlog.WarnWithStr().Any("当前传入的配置文件不存在：", cnfPath).Msg(" 已设置为默认配置文件" + "./conf/default.yaml")
		cnfPath = "./conf/default.yaml"
	}
	// Read YAML file
	yamlFile, err := os.ReadFile(cnfPath)
	if err != nil {
		CheckErrorWithPanic(err, "读取配置文件失败，请检查配置文件：", cnfPath)
	}

	// 将解析后的配置信息转换为具体的结构体
	err = yaml.Unmarshal(yamlFile, configStruct)
	if err != nil {
		CheckErrorWithPanic(err, "无法解析配置文件到结构体", configStruct)
	}
	// Print the parsed configuration
	zlog.InfoWithStr().Any("配置文件为：", configStruct).Msg("")
}

func CheckErrorWithPanic(err error, errDesc string, errInfo interface{}) {
	if err != nil {
		zlog.ErrWithStr(err).Any("error:", errInfo).Msg(errDesc)
	}
}

func CheckError(err error, errDesc string, errInfo interface{}) {
	if err != nil {
		zlog.ErrWithStr(err).Any("error:", errInfo).Msg(errDesc)
	}
}
