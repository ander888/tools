package helper

import (
	"regexp"
)

// “正则匹配” 常见的英文表述是 “Regular Expression Matching” 简称 “Val”
// ValHttpUrl 检测http核https的url，严格检测ip和域名规则，端口号严格控制在10-65535内,http url format: http[s]://1.1.1.1:8080/ass/sss
func ValHttpUrl(httpUrl string) (ok bool) {
	httpUrlReg := `^(https?://)?` + IPOrDomainReg + `(:([10]\d{0,3}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5]))?(/.*)?$`
	// 简单的正则验证，这里假设API URL以http或https开头，后面跟着域名或IP地址以及可选端口等
	apiURLRegex := regexp.MustCompile(httpUrlReg)
	return apiURLRegex.MatchString(httpUrl)
}

// ValIPOrDomain 检测ip或域名是否合规
func ValIPOrDomain(ipOrDomain string) (ok bool) {
	ipOrDomainRegex := regexp.MustCompile(`^` + IPOrDomainReg + `$`)
	return ipOrDomainRegex.MatchString(ipOrDomain)
}

// ValIPOrDomainWithPort 检测ip或域名是否合规
func ValIPOrDomainWithPort(ipOrDomainWithPort string) (ok bool) {
	ipOrDomainRegex := regexp.MustCompile(`^` + IPOrDomainReg + `(:([10]\d{0,3}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5]))?`)
	return ipOrDomainRegex.MatchString(ipOrDomainWithPort)
}

// ValUserName 检测用户名是否合规
func ValUserName(uName string, uNameLen int) (ok bool) {
	// 如果没有设置用户名长度限制，则默认16位
	if uNameLen <= 0 {
		uNameLen = 16
	}

	// 判断用户名长度限制
	if len(uName) < 4 || len(uName) > uNameLen {
		return
	}

	// 严重用户名格式，登录账号 由字母、数字、下划线组成的字符串
	ok = regexp.MustCompile(UsernameReg).MatchString(uName)
	if ok {
		ok = true
	}
	return
}

// ValNickname 用户昵称检测，昵称只支持汉字、字母、数字组成
func ValNickname(nickname string, nicknameLen int) (ok bool) {
	// 如果没有设置昵称长度限制，则默认32位
	if nicknameLen <= 0 {
		nicknameLen = 32
	}

	// 判断昵称长度限制
	if len(nickname) < 4 || len(nickname) > nicknameLen {
		return
	}

	// 昵称格式，昵称只支持汉字、字母、数字组成
	ok = regexp.MustCompile(NicknameReg).MatchString(nickname)
	if ok {
		ok = true
	}
	return
}

// ValPwd 检测密码是否合规,密码长度限制 6个字符以上。默认16位以内
func ValPwd(pwd string, pwdLen int) (ok bool) {
	// 如果没有设置昵称长度限制，则默认16位
	if pwdLen <= 0 {
		pwdLen = 16
	}

	// 判断昵称长度限制
	if len(pwd) < 6 || len(pwd) > pwdLen {
		return
	}
	// 登录密码 由字母、数字、特殊字符等组成
	ok = regexp.MustCompile(PasswordReg).MatchString(pwd)
	if ok {
		ok = true
	}
	return
}

// ValPhoneNumber 按区域验证电话号码是否合规
func ValPhoneNumber(phone, region string) bool {
	var phoneRegex *regexp.Regexp
	switch region {
	case "ChinaMainlandPhNumberRegion":
		phoneRegex = regexp.MustCompile(ChinaMainlandPhNumberReg)
	case "ChinaMacauPhNumberRegion":
		phoneRegex = regexp.MustCompile(ChinaMacauPhNumberReg)
	case "ChinaHongKongPhNumberRegion":
		phoneRegex = regexp.MustCompile(ChinaHongKongPhNumberReg)
	case "ChinaTaiwanPhNumberRegion":
		phoneRegex = regexp.MustCompile(ChinaTaiwanPhNumberReg)
	case "MalaysiaPhNumberRegion":
		phoneRegex = regexp.MustCompile(MalaysiaPhNumberReg)
	case "DubaiPhNumberRegion":
		phoneRegex = regexp.MustCompile(DubaiPhNumberReg)
	case "PhilippinesPhNumberRegion":
		phoneRegex = regexp.MustCompile(PhilippinesPhNumberReg)
	default:
		return false
	}

	return phoneRegex.MatchString(phone)
}

// ValEmailAddr 验证邮箱地址
func ValEmailAddr(emailAddr string) (ok bool) {
	// 判断邮件地址限制
	if len(emailAddr) < 6 || len(emailAddr) > 100 {
		return
	}
	ok = regexp.MustCompile(EmailAddrReg).MatchString(emailAddr)
	if ok {
		ok = true
	}
	return
}

// ValEmailAddr 验证邮箱地址
func ValCarPlateNumber(carPtNmb string) (ok bool, carPlateType string) {

	if regexp.MustCompile(CarPlateNumberICEReg).MatchString(carPtNmb) {
		ok = true
		carPlateType = CarPlateICEType
		return
	} else if regexp.MustCompile(CarPlateNumberNEVReg).MatchString(carPtNmb) {
		ok = true
		carPlateType = CarPlateENVType
		return
	}

	return
}
