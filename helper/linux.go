package helper

import (
	"fmt"
	"os"
)

func CreateShutdownScript(srvName string) error {
	return os.WriteFile("shutdown_"+srvName+".sh", []byte(fmt.Sprintf("kill -9 %d", os.Getpid())), 0777)
}
