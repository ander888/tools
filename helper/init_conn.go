package helper

import (
	"gitee.com/ander888/tools/conn"
	"gitee.com/ander888/tools/model"
	"gitee.com/ander888/tools/zlog"
)

func InitGatewayConn(confPath string) (mnc conn.MinioClient, sqlc conn.SqlClient, edc conn.EtcdClient, err error) {
	cnf := GetGatewayConfByYml(confPath)
	err = zlog.InitLog(cnf.LogCnf.ServiceName, cnf.LogCnf.Path, cnf.LogCnf.StdoutType, cnf.LogCnf.Level)
	if err != nil {
		return mnc, sqlc, edc, err
	}
	mnc = conn.MinioClient{
		Conf: model.MinioConf{Endpoint: cnf.MinioCnf.Endpoint, AccessKeyID: cnf.MinioCnf.AccessKeyID,
			SecretAccessKey: cnf.MinioCnf.SecretAccessKey, UseSSL: cnf.MinioCnf.UseSSL},
	}
	mnc.NewMinioClient()
	if mnc.Err != nil {
		err = mnc.Err
		return mnc, sqlc, edc, err
	}

	sqlc = conn.SqlClient{
		Conf: model.SQLConf{Host: cnf.SqlCnf.Host, Port: cnf.SqlCnf.Port, User: cnf.SqlCnf.User, Pwd: cnf.SqlCnf.Pwd, DBName: cnf.SqlCnf.DBName},
	}
	sqlc.NewSqlDB()
	if sqlc.Err != nil {
		err = edc.Err
		return mnc, sqlc, edc, sqlc.Err
	}
	edc = conn.EtcdClient{Conf: model.EtcdConf{Nodes: cnf.EtcdConf.Nodes, User: cnf.EtcdConf.User, Pwd: cnf.EtcdConf.Pwd}}
	edc.NewEtcdClient()
	if edc.Err != nil {
		err = edc.Err
		return mnc, sqlc, edc, err
	}
	return mnc, sqlc, edc, err
}

func InitScheduleConn(confPath string) (mnc conn.MinioClient, ntc conn.NatsClient, edc conn.EtcdClient, err error) {
	cnf := GetScheduleConfByYml(confPath)
	err = zlog.InitLog(cnf.LogCnf.ServiceName, cnf.LogCnf.Path, cnf.LogCnf.StdoutType, cnf.LogCnf.Level)
	if err != nil {
		return mnc, ntc, edc, err
	}
	mnc = conn.MinioClient{
		Conf: model.MinioConf{Endpoint: cnf.MinioCnf.Endpoint, AccessKeyID: cnf.MinioCnf.AccessKeyID,
			SecretAccessKey: cnf.MinioCnf.SecretAccessKey, UseSSL: cnf.MinioCnf.UseSSL},
	}
	mnc.NewMinioClient()
	if mnc.Err != nil {
		err = mnc.Err
		return mnc, ntc, edc, err
	}

	edc = conn.EtcdClient{Conf: model.EtcdConf{Nodes: cnf.EtcdConf.Nodes, User: cnf.EtcdConf.User, Pwd: cnf.EtcdConf.Pwd}}
	edc.NewEtcdClient()
	if edc.Err != nil {
		err = edc.Err
		return mnc, ntc, edc, err
	}
	return mnc, ntc, edc, err
}

func InitVideoConn(confPath string) (mnc conn.MinioClient, edc conn.EtcdClient, err error) {
	cnf := GetVideoConfByYml(confPath)
	err = zlog.InitLog(cnf.LogCnf.ServiceName, cnf.LogCnf.Path, cnf.LogCnf.StdoutType, cnf.LogCnf.Level)
	if err != nil {
		return mnc, edc, err
	}
	mnc = conn.MinioClient{
		Conf: model.MinioConf{Endpoint: cnf.MinioCnf.Endpoint, AccessKeyID: cnf.MinioCnf.AccessKeyID,
			SecretAccessKey: cnf.MinioCnf.SecretAccessKey, UseSSL: cnf.MinioCnf.UseSSL},
	}
	mnc.NewMinioClient()
	if mnc.Err != nil {
		err = mnc.Err
		return mnc, edc, err
	}

	edc = conn.EtcdClient{Conf: model.EtcdConf{Nodes: cnf.EtcdConf.Nodes, User: cnf.EtcdConf.User, Pwd: cnf.EtcdConf.Pwd}}
	edc.NewEtcdClient()
	if edc.Err != nil {
		err = edc.Err
		return mnc, edc, err
	}
	return mnc, edc, err
}

func InitPredictConn(confPath string) (edc conn.EtcdClient, err error) {
	cnf := GetPredictConfByYml(confPath)
	err = zlog.InitLog(cnf.LogCnf.ServiceName, cnf.LogCnf.Path, cnf.LogCnf.StdoutType, cnf.LogCnf.Level)
	if err != nil {
		return edc, err
	}

	edc = conn.EtcdClient{Conf: model.EtcdConf{Nodes: cnf.EtcdConf.Nodes, User: cnf.EtcdConf.User, Pwd: cnf.EtcdConf.Pwd}}
	edc.NewEtcdClient()
	if edc.Err != nil {
		err = edc.Err
		return edc, err
	}
	return edc, err
}
