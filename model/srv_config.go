package model

const (
	// EnvRelease 环境
	EnvRelease = "rls:"
	EnvDev     = "dev:"
	EnvTest    = "test:"
	// 配置文件
	GatewayConfKey = "gw_ck" // 网关在etcd中的key名称
)

type NatsConf struct {
	Url  string `json:"url" yaml:"url"`   // nats的url
	User string `json:"user" yaml:"user"` // nats用户名
	Pwd  string `json:"pwd" yaml:"pwd"`   // nats的密码
}

// SQLConf DBType数据类型目前支持的有：mysql pgl dameng
type SQLConf struct {
	Host   string `json:"host" yaml:"host"`       // sql型数据库ip
	Port   string `json:"port" yaml:"port"`       // sql型数据库port
	User   string `json:"user" yaml:"user"`       // sql型数据库用户
	Pwd    string `json:"pwd" yaml:"pwd"`         // sql型数据库密码
	DBName string `json:"db_name" yaml:"db_name"` // sql型数据库的名称
	Driver string `json:"driver" yaml:"driver"`   // 数据库类型
}

type LogConf struct {
	ServiceName string `json:"service_name" yaml:"service_name"` // 服务名称
	StdoutType  int    `json:"stdout_type" yaml:"stdout_type"`   // 输出类型 0:终端 1:文件 2:ES
	Path        string `json:"path" yaml:"path"`                 // 日志路径
	Level       int    `json:"level" yaml:"level"`               // 日志级别
}

type MinioConf struct {
	Endpoint        string `json:"endpoint" yaml:"endpoint"`                   // nats的url
	AccessKeyID     string `json:"access_key_id" yaml:"access_key_id"`         // nats用户名
	SecretAccessKey string `json:"secret_access_key" yaml:"secret_access_key"` // nats的密码
	UseSSL          bool   `json:"use_ssl" yaml:"use_ssl"`                     // 是否开启验证
}

type EtcdConf struct {
	Nodes []string `json:"nodes" yaml:"nodes"` // nats的url
	User  string   `json:"user" yaml:"user"`   // nats用户名
	Pwd   string   `json:"pwd" yaml:"pwd"`     // nats的密码
}

type KafkaConf struct {
	Nodes []string `json:"nodes" yaml:"nodes"` // kafka url
	User  string   `json:"user" yaml:"user"`   // kafka 用户名
	Pwd   string   `json:"pwd" yaml:"pwd"`     // kafka的密码
}
