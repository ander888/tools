package conn

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

var (
	queueName    = "ander.queue2"
	msg          = "ander rabbitMQ test"
	exchangeName = "newExchange"
	routeName    = "ander.route1"
	// MQURL amqp://user:password@host:port/vhost
	// amqp://是固定参数，这个信息是固定不变的。后面两个是用户名密码ip地址端口号Virtual Host
	// 如果vhost是“/”就输入/%2F，/%2F代表斜杠
	mqUrl = "amqp://ander:zcs407@localhost:5672/%2F"
)

// -----------------------1、简单模式------------------------
func TestRabbitMQSimplePublish(t *testing.T) {
	rbtMQ := NewRabbitMQSimple(mqUrl, queueName)
	rbtMQ.PublishSimple(msg)
	fmt.Println("发送成功！")
}

func TestRabbitMQSimpleReceive(t *testing.T) {
	// queueName 替换成你自己的队列名
	rbtMQ := NewRabbitMQSimple(mqUrl, queueName)
	msgs := rbtMQ.ConsumeSimple()
	for v := range msgs {
		fmt.Printf("打印消费消息：%s\n", string(v.Body))
	}
}

// -----------------------2、工作模式------------------------
func TestRabbitMQWorkPublish(t *testing.T) {
	rbtMQ := NewRabbitMQWork(mqUrl, queueName)
	rbtMQ.PublishWork(msg)
	fmt.Println("发送成功！")
}

func TestRabbitMQWorkReceive(t *testing.T) {
	// queueName 替换成你自己的队列名
	rbtMQ := NewRabbitMQWork(mqUrl, queueName)
	msgs := rbtMQ.ConsumeWork()
	for v := range msgs {
		fmt.Printf("打印消费消息：%s\n", string(v.Body))
	}
}

// -----------------------3、订阅模式------------------------
func TestRabbitMQPubPublish(t *testing.T) {
	rbtMQ := NewRabbitMQPub(mqUrl, exchangeName)
	for i := 0; i < 100; i++ {
		rbtMQ.PublishPub("订阅模式生产第" +
			strconv.Itoa(i) + "条" + "数据")
		fmt.Println("订阅模式生产第" +
			strconv.Itoa(i) + "条" + "数据")
		time.Sleep(200 * time.Millisecond)
	}
	rbtMQ.PublishPub(msg)
	fmt.Println("发送成功！")
}

func TestRabbitMQPubReceive(t *testing.T) {
	// exchangeName 替换成你自己的队列名
	rbtMQ := NewRabbitMQPub(mqUrl, exchangeName)
	msgs := rbtMQ.ConsumePub()
	for v := range msgs {
		fmt.Printf("打印消费消息：%s\n", string(v.Body))
	}
}

// -----------------------4、路由模式------------------------
func TestRabbitMQRoutePublish(t *testing.T) {
	routingOne := NewRabbitMQRouting(mqUrl, exchangeName+"_route", routeName+"_one")
	routingTwo := NewRabbitMQRouting(mqUrl, exchangeName+"_route", routeName+"_two")

	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			routingOne.PublishRouting(msg + strconv.Itoa(i))
		} else {
			routingTwo.PublishRouting(msg + strconv.Itoa(i))
		}
		time.Sleep(200 * time.Millisecond)
		fmt.Println(msg + strconv.Itoa(i))
	}
}

func TestRabbitMQRouteReceive(t *testing.T) {
	// exchangeName 替换成你自己的队列名
	rbtMQ := NewRabbitMQRouting(mqUrl, exchangeName+"_route", routeName+"_one")
	msgs := rbtMQ.ConsumeRouting()
	for v := range msgs {
		fmt.Printf("打印消费消息：%s\n", string(v.Body))
	}
}

// -----------------------5、Topic模式------------------------
func TestRabbitMQTopicPublish(t *testing.T) {

	topicOne := NewRabbitMQTopic(mqUrl, exchangeName+"_topic", routeName+".one")
	topicTwo := NewRabbitMQTopic(mqUrl, exchangeName+"_topic", routeName+".two")

	for i := 0; i < 100; i++ {
		// 发布消息
		// 参数：交换机，路由键，消息
		topicOne.PublishTopic(msg + strconv.Itoa(i))
		topicTwo.PublishTopic(msg + strconv.Itoa(i))
		// 模拟耗时操作
		time.Sleep(200 * time.Millisecond)
		fmt.Println(msg + strconv.Itoa(i))
	}
}

func TestRabbitMQTopicReceive(t *testing.T) {
	// queueName 替换成你自己的队列名
	rbtMQ := NewRabbitMQTopic(mqUrl, exchangeName+"_topic", routeName+".one")
	msgs := rbtMQ.ConsumeTopic()
	for v := range msgs {
		fmt.Printf("打印消费消息：%s\n", string(v.Body))
	}
}
