package conn

import (
	"context"
	"gitee.com/ander888/tools/model"
	"log"
	"testing"
)

func TestMinioConnErr(t *testing.T) {
	endpoint := "210.120.100.99:9090"
	accessKeyID := "LdzqjZSc9II2JJdELxYq"
	secretAccessKey := "QndrZYJeWZb4Dl4iJfKM6mc6de4yc1gToubpTEht"
	mnCli := new(MinioClient)
	mnCli.Conf = model.MinioConf{
		Endpoint:        endpoint,
		AccessKeyID:     accessKeyID,
		SecretAccessKey: secretAccessKey,
	}
	mnCli.NewMinioClient()
	if mnCli.Err != nil {
		print("链接数据库失败：", mnCli.Err)
		return
	}
	// 检查存储桶是否已经存在。
	exists, err := mnCli.Conn.BucketExists(context.Background(), "video.result.image")
	if err == nil && exists {
		log.Printf("We already own %s\n", "video.result.image")
	} else {
		log.Fatalln(err)
	}

}
