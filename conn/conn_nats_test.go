package conn

import (
	"gitee.com/ander888/tools/model"
	"testing"
	"time"
)

func TestNatsConnErr(t *testing.T) {
	nCli := new(NatsClient)
	nCli.Conf = model.NatsConf{
		Url:  "nats://210.120.100.99:4222",
		User: "",
		Pwd:  "",
	}
	nCli.NewNatsClient()
	if nCli.Err != nil {
		print("链接数据库失败：", nCli.Err)
		return
	}
	defer nCli.Conn.Close()
	sub, _ := nCli.Conn.SubscribeSync("test")
	nCli.QueueSubscribe("test", "", pr)
	nCli.Publish("test", "", "测试消息")
	//nCli.QueueSubscribe("test", "", pr)

	msg, _ := sub.NextMsg(10 * time.Millisecond)
	print("", string(msg.Data))
}

func pr(data []byte) {
	println("------------------------->", string(data))
}
