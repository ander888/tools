package conn

import (
	"context"
	"testing"

	"gitee.com/ander888/tools/zlog"
)

var (
	addr   = "127.0.0.1:6379"
	pwd    = ""
	db     = 2
	chName = "ander.publish.test01"
)

func TestRedisPublish(t *testing.T) {
	rdsC := RedisClient{}
	rdsC.NewRedisClient(addr, pwd, db)
	rdsC.Conn.Publish(context.Background(), chName, "0011aabbccdd")
}

func TestRedisSub(t *testing.T) {
	ctx := context.Background()
	rdsC := RedisClient{}
	rdsC.NewRedisClient(addr, pwd, db)
	pubSub := rdsC.Conn.Subscribe(ctx, chName)

	msg, err := pubSub.ReceiveMessage(ctx)
	if err != nil {
		zlog.Err(err, "")
		return
	}
	zlog.Info(msg.Channel + "=====" + msg.Payload)

}
