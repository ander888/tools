package conn

import (
	"gitee.com/ander888/tools/zlog"
	"github.com/redis/go-redis/v9"
)

type RedisClient struct {
	Conn *redis.Client
}

// 创建redis 客户端
func (rdsC *RedisClient) NewRedisClient(addr, pwd string, db int) {
	rdsC.Conn = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pwd,
		DB:       db,
	})
}

// 销毁redis连接
func (rdsC *RedisClient) Destroy() {
	err := rdsC.Conn.Close()
	if err != nil {
		rdsC.failOnErr(err, "redis 链接销毁失败")
	}

}

// 错误处理函数
func (rdsC *RedisClient) failOnErr(err error, msg string) {
	if err != nil {
		zlog.ErrWithStr(err).Msg(msg)
	}
}

