package conn

import (
	"gitee.com/ander888/tools/model"
	"testing"
)

func TestSqlConnErr(t *testing.T) {
	sqlCli := SqlClient{
		Conf: model.SQLConf{Host: "localhost", Port: "3316", User: "root", Pwd: "151407zcs", DBName: "is_cv_server", Driver: "mysql"},
		Conn: nil,
		Err:  nil,
	}
	sqlCli.NewSqlDB()
	if sqlCli.Err != nil {
		print("链接数据库失败：", sqlCli.Err)
		return
	}

}
