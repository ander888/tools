package conn

import (
	"fmt"
	"log"
	"time"

	"gitee.com/ander888/tools/model"
	"gitee.com/ander888/tools/zlog"
	jsoniter "github.com/json-iterator/go"

	"github.com/nats-io/nats.go"
)

type NatsClient struct {
	Conf model.NatsConf
	Conn *nats.Conn
	Err  error
}

const (
	reconWaitTime                      = 2 * time.Second
	maxReconnects                      = 5
	errCheckFuncInit                   = "NewNatsClient"
	errCheckFuncPublish                = "Publish"
	errCheckFuncSubscribe              = "Subscribe"
	errCheckFuncQueueSubscribe         = "QueueSubscribe"
	errcheckFuncChanSubscribe          = "ChanSubscribe"
	errcheckFuncChanChanQueueSubscribe = "ChanQueueSubscribe"
)

// NewNatsClient 创建nats的客户端
func (nc *NatsClient) NewNatsClient() {
	nc.Conn, nc.Err = nats.Connect(
		nc.Conf.Url,
		nats.DontRandomize(),
		nats.MaxReconnects(maxReconnects),
		nats.ReconnectWait(reconWaitTime),
		nats.ClosedHandler(func(nc *nats.Conn) {
			fmt.Println("close nats connect succ")
		}),
		nats.ErrorHandler(func(conn *nats.Conn, subscription *nats.Subscription, e error) {
			log.Fatalln(nc.Err)
		}))
}

func (n *NatsClient) Close() {
	_ = n.Conn.Close
}

// Publish 发布消息到nats
func (n *NatsClient) Publish(subj, queue string, msg interface{}) {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	data, err := json.Marshal(&msg)
	if !n.checkErr("Publish", subj, queue, err) {
		return
	}
	err = n.Conn.Publish(subj, data)
	n.checkErr("Publish", subj, queue, err)
}

// Subscribe 普通订阅,可多次消费,单节点时使用,多用于goroutine 所以错误日志内部处理
func (n *NatsClient) Subscribe(subj string, msgHandle func(data []byte)) {
	_, err := n.Conn.Subscribe(subj, func(msg *nats.Msg) {
		msgHandle(msg.Data)
	})
	n.checkErr(errCheckFuncSubscribe, subj, "", err)
}

// QueueSubscribe 队列形式的订阅,只消费一次,多服务时使用,多用于goroutine 所以错误日志内部处理
func (n *NatsClient) QueueSubscribe(subj, queue string, msgHandle func(data []byte)) {
	_, err := n.Conn.QueueSubscribe(subj, queue, func(msg *nats.Msg) {
		msgHandle(msg.Data)
	})
	n.checkErr(errCheckFuncQueueSubscribe, subj, queue, err)
}

// ChanSubscribe 通道订阅模式,用于排序处理消息,多次订阅
func (n *NatsClient) ChanSubscribe(subj string, msgHandle func(data []byte)) {
	ch := make(chan *nats.Msg, 1024)
	_, err := n.Conn.ChanSubscribe(subj, ch)
	if !n.checkErr(errcheckFuncChanSubscribe, subj, "", err) {
		return
	}

	for k := range ch {
		msgHandle(k.Data)
	}
}

// ChanQueueSubscribe 通道订阅模式,用于排序处理消息,单次订阅
func (n *NatsClient) ChanQueueSubscribe(subj, queue string, msgHandle func(data []byte)) {
	ch := make(chan *nats.Msg, 1024)
	_, err := n.Conn.ChanQueueSubscribe(subj, queue, ch)
	if !n.checkErr(errcheckFuncChanSubscribe, subj, queue, err) {
		return
	}
	for k := range ch {
		msgHandle(k.Data)
	}

}

// checkErr 检查错误
func (n *NatsClient) checkErr(funcName, subj, queue string, err error) bool {
	if err != nil {
		switch funcName {
		case errCheckFuncPublish:
			zlog.ErrWithStrForErrCheck(err).Str("subj", subj).Msg("nats 发布失败")
		case errCheckFuncSubscribe, errCheckFuncQueueSubscribe, errcheckFuncChanSubscribe,
			errcheckFuncChanChanQueueSubscribe:
			zlog.ErrWithStrForErrCheck(err).Str("subj", subj).Str("queue", queue).Msg("nats 订阅失败")
		default:
		}
		return false
	}
	return true
}
