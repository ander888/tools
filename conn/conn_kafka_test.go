package conn

import (
	"context"
	"fmt"
	"testing"
)

var (
	topicTestName = "alarm-kafka-test01"
	node          = "train01:9092"
	nodes         = []string{"train01:9092"}
)

func TestKafkaTopicCreate(t *testing.T) {
	kfc := new(KfkClient)
	err := kfc.TopicCreate(topicTestName, node)
	if err != nil {
		println("err:", err.Error())
		return
	}
}

func TestKafkaTopicList(t *testing.T) {
	kfc := new(KfkClient)
	err := kfc.TopicList(node)
	if err != nil {
		println("err:", err.Error())
		return
	}
}
func TestKafkaProducer(t *testing.T) {
	kfc := new(KfkClient)
	err := kfc.NewKafkaProducer("", "", topicTestName, node)
	if err != nil {
		println("err:", err.Error())
		return
	}
	err = kfc.Write([]byte("key002"), []byte("msg002"))
	if err != nil {
		println("发送消息到kafka失败", err.Error())
	}
}

func TestKafkaConsumer(t *testing.T) {
	kfc := new(KfkClient)
	err := kfc.NewKafkaConsumer("", "", topicTestName, "", nodes)
	if err != nil {
		println("err:", err.Error())
		return
	}
	for {
		m, err := kfc.Consumer.ReadMessage(context.Background())
		if err != nil {
			println("无法从kafka读取消息", err.Error())
		}

		fmt.Printf("message at offset %d: %s = %s\n", m.Offset, string(m.Key), string(m.Value))
	}
}
