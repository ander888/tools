package zlog

import (
	"fmt"
	"testing"
	"time"
)

func TestErr(t *testing.T) {
	err := InitLog("log", "./", 0, 0)
	if err != nil {
		fmt.Println("test log init failed", err)
	}
	Info("-------------")
	InfoWithStr().Any("key....","这是一个struck").Msg("")
	Warn("+++++++++++++")
	Err(nil,"EEEEEEEEEEE")
}

func TestTimeSet(t *testing.T) {
	// 获取当前时间（UTC）
	utcTime := time.Now().UTC()
	fmt.Println("UTC Time:", utcTime)

	// 加载上海时区
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		fmt.Println("Error loading location:", err)
		return
	}

	// 将 UTC 时间转换为上海时区时间
	shanghaiTime := utcTime.In(loc)
	fmt.Println("Shanghai Time:", shanghaiTime)
}
