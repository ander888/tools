package lic

import (
	"fmt"
	"testing"
)

func TestGenRSAKeyPair(t *testing.T) {
	saveFilePath := "./"
	expirationDays := 30 // 30天有效期
	// 生成公钥私钥
	if err := GenRSAKeyPair(saveFilePath); err != nil {
		fmt.Println(err)
		return
	}
	// 生成私钥加密后的license文件
	if err := GenEncryptLicenseFile(saveFilePath, expirationDays); err != nil {
		fmt.Println(err)
		return
	}

	//time.Sleep(time.Second * 10)
	// 验证
	licenseFilePath, privateKeyFilePath := fmt.Sprintf("%s/%s", saveFilePath, LicenseFileName),
		fmt.Sprintf("%s/%s", saveFilePath, PrivateKeyFileName)
	license, err := VerifyLicense(licenseFilePath, privateKeyFilePath)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("license文件是否有效：", license)
}
